# TP SoapUI
1. [L'age d'or des APIs](#age-d-or)
2. [Objectif des tests fonctionnels](#objectifs)
3. [Introduction à SoapUI](#intro-soapui)
4. [Prise en main avec Soap et XML](#prise-en-main)
5. [REST et JSON, Test de l'API Google Map](#rest-json)
6. [Mocking](#mocking)


## <a name="age-d-or"></a>L'age d'or des APIs

La création et l'utilisation des interfaces de programmation est un sujet incontournable de la programmation contemporaine. Une application se sert typiquement de nombreuses interfaces de programmation; mises en œuvre par des bibliothèques logicielles qui peuvent elles-mêmes se servir d'autres interfaces de programmation.

En architecture orientée services les applications peuvent dépendre de fonctionnalités tierces offertes par des logiciels via des interfaces de programmation mises en œuvre par des services web.

![enter image description here](http://downloads.eviware.s3.amazonaws.com/web_site_images/soapui/web_images/Dojo/programmableweb_graf01%20%281%29.png)

Les APIs permettent les interactions entres applications depuis les années 90. Avec l'évolution du Web et le besoin de disposer d'applications interconnectées, on rentre aujourd'hui dans ce qu'on appelle l'age d'or des APIs.

Une API permet d’exposer à des clients des méthodes et des objets de manière simple, mais le client d’une API doit être assuré de la **stabilité** des **signatures** et du comportement pour une version mineure donnée. Il est donc intéressant de vérifier qu’une API reste stable et respecte son contrat d’interface dans le temps. Pour cela il faut créer des tests fonctionnels et automatiser leur lancement pour qu’ils soient exécutés lors de l’**intégration continue**. 

*Aller plus loin*:
http://www.soapui.org/testing-dojo/world-of-api-testing/the-golden-age-of-apis.html

## <a name="objectifs"></a>Objectifs des tests fonctionnels & de non regression

> “Continuous Integration doesn’t get rid of bugs, but it does make them
> dramatically easier to find and remove.”

[Source Thoughtworks](https://www.thoughtworks.com/continuous-integration)

L'intégration continue repose souvent sur la mise en place d'une brique logicielle permettant **l'automatisation** de tâches : compilation, tests unitaires et fonctionnels, validation produit, tests de performances, etc.

Parmi les bonnes pratiques, on retrouve certains objetifs qu'il devient facile de remplir avec un outil comme SoapUI : 

 - Automatiser les compilations et le déploiement
 - Maintenir un cycle de compilation court
 - Tester dans un environnement de production cloné
 - Rendre disponible facilement le dernier exécutable 
 - Automatiser le déploiement

SoapUI, en se basant sur les contrats (*WSDL* ou *WADL*), permet de créer rapidement de nouveaux cas de tests, avant même que le travail de développement soit commencé (*Contract First*, *Test Driven Development*).

Les assertions, points de contrôle, qui vérifient qu'un contrat est respecté s'attacheront à comparer les codes de retours des appels, en commençant principalement par les cas nominaux (le cas standard représentant 80% des appels effectués). 

La *variabilisation* des données (*Data Driven Testing*) ainsi que le changement d'endpoint permettent de rejouer facilement une suite de tests sur plusieurs environnements.

## <a name="intro-soapui"></a>Introduction à SoapUI

SoapUI permet de mettre en place une suite de tests qui peuvent être lancé d’une traite du côté client, permet de tester les services web en mode bouchon mais aussi d’effectuer des tests de charge. Aussi, Il dispose de plusieurs fonctionnalités qui font de lui un bon allié pour les développeurs et les testeurs : 

 - Tests fonctionnels et de non régression,
 - Tests de sécurité automatique,
 - Test de charge - avec LoadUI, 
 - Mock d'un service 
 - Intégration au processus de build

*Aller plus loin* :
[SoapUI, le couteau suisse du testeur](http://www.soapui.org/about-soapui/what-is-soapui-.html)

## <a name="prise-en-main"></a>Exemple 1 - Prise en main avec Soap et XML

#### Installation : 
 - Télécharger et installer SoapUI :
   http://www.soapui.org/getting-started/installing-soapui/
   
 - Créer un nouvel espace de travail, workspace.

 - Créer un nouveau projet SOAP en ajoutant le lien du wsdl suivant
 http://www.webservicex.net/stockquote.asmx?wsdl
 
![Création d'un projet SOAP](http://gitlab.com/gtheraud/tp-iut/raw/master/images/1.png)

#### L'interface utilisateur :

![Les projets, interfaces, operations](http://gitlab.com/gtheraud/tp-iut/raw/master/images/2.png)

![Création d'un projet SOAP](http://gitlab.com/gtheraud/tp-iut/raw/master/images/3.png)

![Assertions](http://gitlab.com/gtheraud/tp-iut/raw/master/images/4.png)

#### Première suite de tests et premier cas de test

 - Créer une nouvelle suite de test et y ajouter un cas de test nominal.

 - On verifiera qu'une requête contenant le corps de message suivant est correctement interprétée par le serveur

>     <web:GetQuote> 
>     <web:symbol>AAPL</web:symbol> 
>     </web:GetQuote>

 - Les points de contrôles, **assertions**, permettront de vérifier que le serveur a renvoyé une réponse SOAP Valide et que cette réponse contient le message *Apple*. SoapUI supporte le langage XPath qui permet de naviguer dans du XML.
 
![Assertions](http://gitlab.com/gtheraud/tp-iut/raw/master/images/5.png)


 - Dans un second cas de test, vérifier ensuite la requête pour le symbole "*?*" renvoie un message : "*Exception*".
 
 - Enfin, sauvegarder le projet puis, dans le menu, lancer *Project > Launch Test Runner* et activer l'export des résultats aux format JUnit
![Assertions](http://gitlab.com/gtheraud/tp-iut/raw/master/images/6.png)


#### Definitions :

 - *Web Service* : une application capable de recevoir et de transmettre des données entre des applications et systèmes hétérogènes. 
 - *SOAP (Simple Object Access Protocol)* qui est un protocole de RPC, basé sur le langage XML et utilisé pour communiquer entre des systèmes hétérogènes.
 - *WSDL (Web Service Description Language)* est un format XML servant à décrire un web service, elle décrit la structure de celui-ci ; le nom des fonctions, les paramètres attendus, etc… C'est le **contrat** qui doit être testé.


## <a name="rest-json"></a>Exemple 2 : REST et JSON, Test de l'API Google Map

#### Contexte : 

> REST est un style d’architecture qui repose sur le protocole HTTP : On
> accède à une ressource (par son URI unique) pour procéder à diverses
> opérations (GET lecture / POST écriture / PUT modification / DELETE
> suppression), opérations supportées nativement par HTTP.

L'objectif de cet exemple est de vérifier que la **méthode de calcul de distance entre deux points est correctement implémentée**.

Pour se faire, on vérifiera les 4 cas de tests suivants : 

 1. Nominal_A - `Ville1 = Vannes` et `Ville2 = Brest`, alors `distance.value = 184377` & `code retour = 200`
 2. Nominal_B - `Ville1 = Brest` et `Ville2 = Vannes`, alors `distance.value = 184377` & `code retour = 200`
 3. VilleManquante - `Ville1 = Vannes` et `Ville2 = NULL`, alors `code retour = 500`
 4. VillesEgales - `Ville1 = Vannes` et `Ville2 = Vannes`, alors `distance.value = 0`

![JsonPatchMatch](http://gitlab.com/gtheraud/tp-iut/raw/master/images/11.png)

#### Application :

 - Télécharger et sauvegarde le fichier [DistanceMatrixAPI.wadl](http://gitlab.com/gtheraud/tp-iut/raw/master/DistanceMatrixAPI.wadl)
 - Créer un nouveau projet REST importer le fichier précédemment sauvegardé
 - Ajouter une suite de test et un cas de test qui vérifie que la distance entre Vannes et Brest est supérieure à 0 (Modifier les valeurs pour les paramètres *origins* et *destinations*).
 
    ```
    $.rows[0].elements[0]
    ```
 
 - On utilisera l'assertion de type `JsonPath Match`: 
 
![JsonPatchMatch](http://gitlab.com/gtheraud/tp-iut/raw/master/images/10.png)
 
 - Vérifier que le code de retour du serveur est 200 (Utiliser l'assertion de type *Valid HTTP Status Codes*).
 - Variabiliser ensuite les 2 villes en utilisant des propriétés personnalisées
 
![Properties](http://gitlab.com/gtheraud/tp-iut/raw/master/images/7.png)

 - Modifier la requête pour utiliser ses variables de projets en utilisant la notation `${#Scope#VariableName}` où `Scope` vaut `Project`, `TestSuite` ou `TestCase`.

 - Créer une nouvelle opération qui utilise le verbe DELETE et qui est similaire à la première.
 - Vérifier ensuite l'API Google ne nous permet pas d'effectuer une opération de suppression.


## <a name="mocking"></a>Exemple 3 : Mocking

#### Définition : 

> En programmation orientée objet, les mocks (ou mock object) sont des
> objets simulés qui reproduisent le comportement d'objets réels de
> manière contrôlée. Un programmeur crée un mock dans le but de tester
> le comportement d'autres objets, réels, mais liés à un objet
> inaccessible ou non implémenté. Ce dernier est alors remplacé par un
> mock.

[Source Wikipedia](https://fr.wikipedia.org/wiki/Mock_%28programmation_orient%C3%A9e_objet%29)

#### Contexte : 

Maintenant que nous avons vu comment créer un projet, une suite de tests, un cas de test et une assertion, appliquons ces éléments à un cas plus concret.

Dans le TP sur la création d'un référentiel d'exigences, il était question de permettre la création d'une fiche simple pour un salarié en renseignant :  

>  - un ID, 
>  - un nom, 
>  - un prénom, 
>  - un nombre de points,
>  - et un salaire.


Dans une API REST, cela peut se traduire pas la requête de type POST (création) avec pour contenu, le body de type application/json suivant : 

    {
    	"id": "FE4872",
    	"nom": "Martin",
    	"prenom": "Pierre",
    	"points": "15",
    	"salaire": "38000"
    }

#### Application :

 - Importer le projet [salaries-soapui-project.xml](https://gitlab.com/gtheraud/tp-iut/raw/master/salaries-soapui-project.xml).
 - Démarrer le mock qui expose une route pour la méthode GET à l'adresse : http://localhost:8080/salarie
 - Ajouter un cas de test qui vérifie que l'ensemble des éléments (id, nom, prénom, point et salaire) sont nécessaire à la création d'un nouvel individus. On vérifiera que le code réponse est 200.
 - Ajouter un cas de test qui vérifie qu'en cas d'erreur, le code retour est 400 (bad request)


![Mock](http://gitlab.com/gtheraud/tp-iut/raw/master/images/9.png)